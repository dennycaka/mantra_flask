# File to seed data to the Mantra database
from mantra import db
from mantra.models import Platform, StatisticYear
from datetime import datetime

print('Dropping existing database tables...')
db.drop_all()

print('Creating database tables...')
db.create_all()

# Platform that being stored to database
platform1 = Platform(type='wordpress', signature='<.*=(.*/wp-).*/.*>')
platform2 = Platform(type='joomla', signature='["\']Joomla')

# Store the current year
year1 = StatisticYear(year=datetime.utcnow().year)

print('Seeding data...')
db.session.add_all([platform1, platform2])
db.session.add_all([year1])


# Commit the changes
db.session.commit()
print('Done!')
