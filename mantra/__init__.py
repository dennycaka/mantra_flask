# Initiate Mantra Website Application

# ==================== #
#     Requirements     #
# ==================== #

# Import OS module
import os

# Import Flask Microframework
from flask import Flask
# Import Flask extension providing bcrypt hasing and comparison facilities
from flask_bcrypt import Bcrypt
# Import user session management for Flask
from flask_login import LoginManager
# Import Marshmallow for Flask
from flask_marshmallow import Marshmallow
# Import SQLAlchemy for Flask
from flask_sqlalchemy import SQLAlchemy

# ==================== #


# Initiating directory path
basedir = os.path.abspath(os.path.dirname(__file__))

# Initiating Mantra Website Application
app = Flask(__name__)

# Mantra configuration
app.config['SECRET_KEY'] = '9fd4804b15fd9e1d36192e5537401c04697b3462c63ee7f091f16c4e84919255'
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(basedir, 'mantra.db')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False


# Initiating Mantra elements
bcrypt = Bcrypt(app)
db = SQLAlchemy(app)
login_manager = LoginManager(app)
marsh = Marshmallow(app)

# Login manager configuration
login_manager.login_view = 'auth.login'
login_manager.login_message = None


# Import a module/component using its blueprint handler variable
from mantra.mod_auth.controllers import mod_auth as auth_module
from mantra.mod_error.handlers import mod_errors as error_module
from mantra.mod_main.controllers import mod_main as main_module
from mantra.mod_pentest.controllers import mod_pentest as pentest_module


# Register the route blueprints
app.register_blueprint(auth_module)
app.register_blueprint(error_module)
app.register_blueprint(main_module)
app.register_blueprint(pentest_module)