# Forms for Mantra auth module

# ====================== #
#      Requirements      #
# ====================== #

# Import requirement of session management
from flask_login import current_user
# Import FlaskForm module
from flask_wtf import FlaskForm
# Import elements for file
from flask_wtf.file import FileAllowed, FileField
# Import form field elements
from wtforms import BooleanField, PasswordField, StringField, SubmitField
# Import validators for fields
from wtforms.validators import (DataRequired, Email, EqualTo, Length,
                                ValidationError)
# Import elements from Mantra Application
from mantra import bcrypt
# Import models for the module
from mantra.models import User

# ====================== #


# ===================== #
#     Register Form     #
# ===================== #
class RegistrationForm(FlaskForm):
    # Attribute
    username = StringField('Username', validators=[
                           DataRequired(), Length(min=2, max=25)])
    name = StringField('Name', validators=[DataRequired()])
    email = StringField('Email', validators=[DataRequired(), Email()])
    password = PasswordField('Password', validators=[
                             DataRequired(), Length(min=5)])
    confirm_password = PasswordField('Confirm Password', validators=[
                                     DataRequired(), EqualTo('password')])
    submit = SubmitField('Sign Up')
    # Validators
    def validate_username(self, username):
        user = User.query.filter_by(username=username.data).first()
        if user:
            raise ValidationError("The username is already taken.")
    def validate_email(self, email):
        user = User.query.filter_by(email=email.data).first()
        if user:
            raise ValidationError("The email is already taken.")


# ================== #
#     Login Form     #
# ================== #
class LoginForm(FlaskForm):
    # Attribute
    username = StringField('Username', validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired()])
    remember = BooleanField('Remember Me')
    submit = SubmitField('Log In')


# =========================== #
#     Update Profile Form     #
# =========================== #
class UpdateProfileForm(FlaskForm):
    # Attribute
    username = StringField('Username', validators=[
                           DataRequired(), Length(min=2, max=25)])
    name = StringField('Name', validators=[DataRequired()])
    email = StringField('Email', validators=[DataRequired(), Email()])

    new_password = PasswordField('New Password')

    picture = FileField('Profile Picture', validators=[
                        FileAllowed(['jpg', 'png'])])
    submit = SubmitField('Update Profile')
    # Validators
    def validate_username(self, username):
        if username.data != current_user.username:
            user = User.query.filter_by(username=username.data).first()
            if user:
                raise ValidationError("The username is already taken.")
    def validate_email(self, email):
        if email.data != current_user.email:
            user = User.query.filter_by(email=email.data).first()
            if user:
                raise ValidationError("The email is already taken.")
    def validate_new_password(self, new_password):
        if new_password.data:
            if len(new_password.data) < 5:
                raise ValidationError("Field must be at least 5 characters long.")
        if bcrypt.check_password_hash(current_user.password, new_password.data):
            raise ValidationError("Password cannot be the same as the old one.")
