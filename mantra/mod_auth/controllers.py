# Auth module for Mantra Application

# ==================== #
#     Requirements     #
# ==================== #

# Import secrets module
import secrets

# Import dependencies from flask module
from flask import Blueprint, flash, redirect, render_template, request, url_for
# Import requirement of session management
from flask_login import current_user, login_required, login_user, logout_user

# Import elements from Mantra Application
from mantra import bcrypt, db
# Import forms required in the module
from mantra.mod_auth.forms import (LoginForm, RegistrationForm,
                                   UpdateProfileForm)
# Import utilities required in the module
from mantra.mod_auth.utils import save_picture
# Import models required in the module
from mantra.models import User

# ==================== #


# Define the blueprint: 'auth', set its url prefix: app.url/auth
mod_auth = Blueprint('auth', __name__, url_prefix='/auth')


# Set the routes and accepted methods

# ====================== #
#     Register Route     #
# ====================== #
@mod_auth.route('/register', methods=['GET', 'POST'])
def register():
    form = RegistrationForm()
    # If user already authenticated
    if current_user.is_authenticated:
        return redirect(url_for('main.home'))
    # If form is submitted
    if form.validate_on_submit():
        # Generate user_token
        random_token = secrets.token_hex(32)
        # Hashing user password
        hashed_pwd = bcrypt.generate_password_hash(
            form.password.data).decode('utf-8')
        # Create new User
        username = form.username.data
        name = form.name.data
        email = form.email.data
        password = hashed_pwd
        user_token = random_token
        new_user = User(username, name, email, password, user_token)
        # Store new user and apply changes to database
        db.session.add(new_user)
        db.session.commit()
        flash(f"A new account has been created!", 'success')
        return redirect(url_for('auth.login'))
    return render_template('auth/register.html', form=form,
                           title='Register', adds='register-page')


# =================== #
#     Login Route     #
# =================== #
@mod_auth.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm()
    # If user already authenticated
    if current_user.is_authenticated:
        return redirect(url_for('main.home'))
    # If form is submitted
    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()
        # If login success
        if user and bcrypt.check_password_hash(user.password, form.password.data):
            # Session is for user
            login_user(user, remember=form.remember.data)
            flash(f"Login success!", 'success')
            next_page = request.args.get('next')
            return redirect(next_page) if next_page else redirect(url_for('main.home'))
        # If login is not success
        else:
            flash(f"These credentials do not match our records.", 'danger')
    return render_template('auth/login.html', form=form,
                           title='Login', adds='login-page')


# ==================== #
#     Logout Route     #
# ==================== #
@mod_auth.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('main.home'))


# =================== #
#    Profile Route    #
# =================== #
@mod_auth.route('/profile', methods=['GET', 'POST'])
@login_required
def profile():
    form = UpdateProfileForm()
    # If form is submitted
    if form.validate_on_submit():
        # Save profile picture
        if form.picture.data:
            picture_file = save_picture(form.picture.data)
            current_user.image_file = picture_file
        if form.new_password.data:
            hashed_pwd = bcrypt.generate_password_hash(form.new_password.data).decode('utf-8')
            current_user.password = hashed_pwd
        current_user.username = form.username.data
        current_user.name = form.name.data
        current_user.email = form.email.data
        # Apply changes to database
        db.session.commit()
        flash(f"Profile Updated!", 'success')
        return redirect(url_for('auth.profile'))
    elif request.method == 'GET':
        form.username.data = current_user.username
        form.name.data = current_user.name
        form.email.data = current_user.email
    return render_template('auth/profile.html', form=form,
                           title='Profile', profile_menu='active',
                           icons='user')
