# mantra/test_login.py

# Import os
import os
# Import unittest as base for unit testing
import unittest
# Import elements from Mantra Application
from mantra import app, basedir, db


TEST_DB = 'test.db'


class BasicTests(unittest.TestCase):

    ############################
    #### setup and teardown ####
    ############################

    # executed prior to each test
    def setUp(self):
        app.config['TESTING'] = True
        app.config['WTF_CSRF_ENABLED'] = False
        app.config['DEBUG'] = False
        app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(basedir, TEST_DB)
        with app.app_context():
            db.create_all()
        self.app = app.test_client()

    # executed after each test
    def tearDown(self):
        db.drop_all()

    ########################
    #### helper methods ####
    ########################

    def register(self):
        return self.app.post(
            '/auth/register',
            data=dict(
                username='user1',
                name='user1',
                email='user1@test.com',
                password='test123456',
                confirm_password='test123456'
            ),
            follow_redirects=True
        )

    def login(self, username, password, remember):
        return self.app.post(
            '/auth/login',
            data=dict(
                username=username,
                password=password,
                remember=remember
            ),
            follow_redirects=True
        )

###############
#### tests ####
###############

    # Test valid user login
    def test_1_valid_user_login(self):
        response_register = self.register()
        self.assertEqual(response_register.status_code, 200)
        print('Test valid user login')
        response_login = self.login('user1', 'test123456', True)
        self.assertEqual(response_login.status_code, 200)
        self.assertIn(b'Login success!', response_login.data)

    # Test invalid user login with invalid passwords
    def test_2_invalid_user_login_invalid_passwords(self):
        print('Test invalid user login with invalid passwords')
        response = self.login('user1', 'test12345678', False)
        self.assertIn(b'These credentials do not match our records.', response.data)

if __name__ == "__main__":
    unittest.main()