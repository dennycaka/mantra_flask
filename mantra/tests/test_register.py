# mantra/test_register.py

# Import os
import os
# Import unittest as base for unit testing
import unittest
# Import elements from Mantra Application
from mantra import app, basedir, db


TEST_DB = 'test.db'


class BasicTests(unittest.TestCase):

    ############################
    #### setup and teardown ####
    ############################

    # executed prior to each test
    def setUp(self):
        app.config['TESTING'] = True
        app.config['WTF_CSRF_ENABLED'] = False
        app.config['DEBUG'] = False
        app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(basedir, TEST_DB)
        with app.app_context():
            db.create_all()
        self.app = app.test_client()

    # executed after each test
    def tearDown(self):
        db.drop_all()

    ########################
    #### helper methods ####
    ########################

    def register(self, username, name, email, password, confirm_password):
        return self.app.post(
            '/auth/register',
            data=dict(
                username=username,
                name=name,
                email=email,
                password=password,
                confirm_password=confirm_password
            ),
            follow_redirects=True
        )

###############
#### tests ####
###############

    # Test valid user register
    def test_1_valid_user_register(self):
        print('Test valid user register')
        response_register = self.register('user1', 'user1', 'user1@test.com', 'test123456', 'test123456')
        self.assertEqual(response_register.status_code, 200)
        self.assertIn(b'A new account has been created!', response_register.data)

    # Test invalid user registration with different passwords
    def test_2_invalid_user_registration_different_passwords(self):
        print('Test invalid user registration with different passwords')
        response = self.register('user2', 'user2', 'user2@test.com', 'test123456', 'test12345678')
        self.assertIn(b'Field must be equal to password.', response.data)

if __name__ == "__main__":
    unittest.main()