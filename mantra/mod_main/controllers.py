# Main module for Mantra Application

# ==================== #
#     Requirements     #
# ==================== #

# Import dependencies from flask module
from flask import Blueprint, jsonify, redirect, render_template, url_for
# Import requirement of session management
from flask_login import current_user, login_required

# Import forms required in the module
from mantra.mod_main.forms import StatisticYearForm
# Import utilities required in the module
from mantra.mod_main.utils import (generate_monthly_statistic,
                                   generate_platform_statistic)
# Import models required in the module
from mantra.models import ScannedWebSchema

# ==================== #


# Define the blueprint: 'main', set its url prefix: app.url/
mod_main = Blueprint('main', __name__, url_prefix='/')


# Set the routes and accepted methods

# ================== #
#     Home Route     #
# ================== #
@mod_main.route('/', methods=['GET', 'POST'])
@mod_main.route('/home', methods=['GET', 'POST'])
@login_required
def home():
    form = StatisticYearForm()
    return render_template('main/home.html', title='Dashboard', form=form,
                           home_menu='active', icons='pie-chart')


# =========================== #
#     Documentation Route     #
# =========================== #
@mod_main.route('/documentation')
@login_required
def documentation():
    return render_template('main/documentation.html', title='Documentation',
                           doc_menu='active', icons='book')


# ============================= #
#     About Developer Route     #
# ============================= #
@mod_main.route('/developers')
@login_required
def developers():
    return render_template('main/developer.html', title='Developers',
                           dev_menu='active', icons='legal')


# =========================== #
#     Documentation Route     #
# =========================== #
@mod_main.route('/pentest_chart/<int:year>')
@login_required
def pentest_chart(year):
    # scanned_webs_schema = ScannedWebSchema(many=True)
    scanned_webs = current_user.scanned_webs
    web_list = []
    for web in scanned_webs:
        if web.date_created.year == year:
            data = web
            web_list.append(data)
    platform_stat = generate_platform_statistic(web_list)
    monthly_stat = generate_monthly_statistic(web_list)
    obj = {
        "platform_statistic": platform_stat,
        "monthly_statistic": monthly_stat,
    }
    return jsonify(statistic=obj)