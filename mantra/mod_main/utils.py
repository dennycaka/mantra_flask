# Utilities for Mantra main module

# ==================== #
#     Requirements     #
# ==================== #

# Import calendar module
import calendar
# Import random module
import random
# Import Counter module
from collections import Counter
# Import datetime module
from datetime import datetime

# ==================== #


# ================================ #
#     Randomize Color Function     #
# ================================ #
def random_color():
    def r(): return random.randint(100, 255)
    result = ("#%02X%02X%02X" % (r(), r(), r()))
    return result


# =================================== #
#     Platform Statistic Function     #
# =================================== #
def generate_platform_statistic(scanned_webs):
    platform_list = []
    for i in scanned_webs:
        platform_type = i.platform.type
        platform_list.append(platform_type)
    count = Counter()
    statistic_data = []
    for x in platform_list:
        count[x] += 1
    generated_data = list(count.items())
    for i in generated_data:
        platform_obj = {'platform': i[0],
                        'count': i[1], 'color': random_color()}
        statistic_data.append(platform_obj)
    return statistic_data


# ================================== #
#     Monthly Statistic Function     #
# ================================== #
def generate_monthly_statistic(scanned_webs):
    months_list = [month for month in calendar.month_abbr if month != ""]
    count = Counter()
    web_list = []
    for web in scanned_webs:
        data = {"platform": web.platform.type,
                "month": web.date_created.strftime('%b')}
        web_list.append(data)
    platform_per_month = []
    for web in web_list:
        count[web['platform'], web['month']] += 1
    generated_data = list(count.items())
    for data in generated_data:
        platform_obj = {'platform': data[0][0],
                        'month': data[0][1], 'count': data[1]}
        platform_per_month.append(platform_obj)
    statistic_data = []
    seen_data = set(statistic_data)
    for platform in platform_per_month:
        plat_name = platform['platform']
        data_per_month = [0]*len(months_list)
        for data in platform_per_month:
            if plat_name == data['platform']:
                month_name = data['month']
                for month in range(len(months_list)):
                    if months_list[month] == month_name:
                        data_per_month[month] = data['count']
        if plat_name not in seen_data:
            seen_data.add(plat_name)
            data_obj = {'platform': plat_name, 'data': data_per_month}
            statistic_data.append(data_obj)
    datasets = []
    for data in statistic_data:
        bg_color = random_color()
        stat_data = {
            "label": data['platform'],
            "borderColor": bg_color,
            "backgroundColor": bg_color,
            "fill": False,
            "data": data['data']
        }
        datasets.append(stat_data)
    sent_data = {
        "labels": months_list,
        "datasets": datasets,
    }
    return sent_data
