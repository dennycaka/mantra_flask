# Declaring models to be defined in the database

# ==================== #
#     Requirements     #
# ==================== #

# Import datetime module
from datetime import datetime

# Import UserMixin object
from flask_login import UserMixin

# Import objects from Mantra Application
from mantra import app, db, login_manager, marsh

# ==================== #


# Define a base model for other database tables to inherit
class Base(db.Model):
    __abstract__ = True
    __table_args__ = {'extend_existing': True}
    # Attribute
    id = db.Column(db.Integer, primary_key=True)
    # Timestamps
    date_created = db.Column(db.DateTime, default=datetime.utcnow)
    date_modified = db.Column(
        db.DateTime, default=datetime.utcnow, onupdate=datetime.utcnow)


# ================================ #
#     Handle load user session     #
# ================================ #
@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))


# ============================= #
#     User model and schema     #
# ============================= #
class User(Base, UserMixin):
    # Table name
    __tablename__ = 'user'
    # Attributes
    username = db.Column(db.String(20), unique=True, nullable=False)
    name = db.Column(db.String(128), nullable=False)
    email = db.Column(db.String(128), unique=True, nullable=False)
    password = db.Column(db.String(60), nullable=False)
    image_file = db.Column(db.String(20), nullable=False, default='default.png')
    user_token = db.Column(db.String(64), unique=True, nullable=False)
    # Relationship
    scanned_webs = db.relationship('ScannedWeb', backref='tester', lazy=True)
    # Constructor
    def __init__(self, username, name, email, password, user_token):
        self.username = username
        self.name = name
        self.email = email
        self.password = password
        self.user_token = user_token
    # Representation
    def __repr__(self):
        return (f"User {self.username} ('{self.name}', {self.email})")


# ==================================== #
#     Scanned Web model and schema     #
# ==================================== #
class ScannedWeb(Base):
    # Table name
    __tablename__ = 'scanned_web'
    # Attributes
    scan_id = db.Column(db.String(128), unique=True, nullable=False)
    title = db.Column(db.String(128), nullable=False)
    url = db.Column(db.Text, nullable=False)
    description = db.Column(db.Text, nullable=False)
    status = db.Column(db.Integer, nullable=False, default=0)
    raw_result = db.Column(db.Text, nullable=False)
    # Relationship
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    platform_id = db.Column(db.Integer, db.ForeignKey(
        'platform.id'), nullable=False)
    # Constructor
    def __init__(self, title, url, description):
        self.title = title
        self.url = url
        self.description = description
    # Representation
    def __repr__(self):
        return (f"Scanned Web '{self.scan_id}', (url: '{self.url}', title: '{self.title}').")


# ================================= #
#     Platform model and schema     #
# ================================= #
class Platform(Base):
    # Table name
    __tablename__ = 'platform'
    # Attributes
    type = db.Column(db.String, nullable=False)
    signature = db.Column(db.Text, nullable=False)
    # Relationship
    scanned_webs = db.relationship('ScannedWeb', backref='platform', lazy=True)
    # Constructor
    def __init__(self, type, signature):
        self.type = type
        self.signature = signature
    # Representation
    def __repr__(self):
        return (f"Platform '{self.type}', ({self.signature})")


# ============================ #
#     SQLmap Service model     #
# ============================ #
class SqlmapService(Base):
    # Table name
    __tablename__ = 'sqlmap_service'
    # Attribute
    status = db.Column(db.String, nullable=False, default='deactivated')
    # Constructor
    def __init__(self, status):
        self.status = status
    # Representation
    def __repr__(self):
        return (f"SQLmap '{self.status}'")


# ============================ #
#     Statistic Year model     #
# ============================ #
class StatisticYear(Base):
    # Table name
    __tablename__ = 'statistic_year'
    # Attribute
    year = db.Column(db.String(4), nullable=False)
    # Constructor
    def __init__(self, year):
        self.year = year
    # Representation
    def __repr__(self):
        return (f"Year: {self.year}")


# =====================#
#     Model schema     #
# =====================#
class UserSchema(marsh.ModelSchema):
    class Meta:
        model = User
        fields = ('username','name', 'email')

class PlatformSchema(marsh.ModelSchema):
    class Meta:
        model = Platform
        fields = ('type', 'signature')

class ScannedWebSchema(marsh.ModelSchema):
    class Meta:
        model = ScannedWeb
        fields = ('scan_id', 'title', 'url', 'description',
                    'status', 'raw_result', 'date_created', 'date_modified', 'tester', 'platform')
    tester = marsh.Nested(UserSchema, only=('username'))
    platform = marsh.Nested(PlatformSchema, only=('type'))