import re
import json

class JoomlaJSONFormat:
    def __init__(self, result):
        self.result = result
    def pretty_print(self):
        blacklist = ['[31m', '[34m','[3;J[H[2J[33m','[33m', '[0m']
        data = self.result.replace(blacklist[0], '').replace(blacklist[1], '').replace(blacklist[2], '').replace(blacklist[3], '').replace(blacklist[4], '')
        data = data.split("@OWASP\n", 1)[1].split("Your Report", 1)[0].replace("\n", "<br>")
        return json.dumps(data)