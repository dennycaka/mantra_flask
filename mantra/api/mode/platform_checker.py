#!/usr/bin/python3

import requests
import re
from db.db import DBConnect

db_connect = DBConnect()

class Checker:
    def __init__(self, url):
        self.url = url

    def platform_check(self):	
        req = requests.get(self.url)
        conn = db_connect.connect()
        query = conn.execute("SELECT * FROM platform") 
        columns = [d[0] for d in query.cursor.description]
        datas = [dict(zip(columns, row)) for row in query.cursor.fetchall()]
        for data in datas:
            sig_check = re.findall(data['signature'], req.text)
            if sig_check:
                return data['id'], data['type']
        return 'webapp'