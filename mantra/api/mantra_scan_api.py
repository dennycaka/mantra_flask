#!/usr/bin/python3

from flask import Flask, request, jsonify, send_from_directory, Blueprint
from flask_restful import Resource, Api
from json import dumps, loads
from flask_jsonpify import jsonify
from subprocess import check_output, CalledProcessError, Popen, STDOUT, PIPE
import configparser
from db.db import DBConnect
from core.sqlmap_service import SQLMapService
from multiprocessing.pool import ThreadPool
import datetime
import random

import os.path
from flask import Flask
from flask_autoindex import AutoIndex, AutoIndexApplication

from mode.platform_checker import Checker

# from sqlalchemy import create_engine
# mod_errors = Blueprint('errors', __name__)

# from mantra.models import *

app = Flask(__name__, static_url_path='')
api = Api(app)

config = configparser.ConfigParser()
config.read('config.ini')

db_connect = DBConnect()

# conn = db_connect.connect()
# # # Create table
# # conn.execute('''CREATE TABLE sqlmap_service
# #              (status text)''')
# query = conn.execute("SELECT status FROM sqlmap_service")

# print(query.fetchone()[0])
# # Insert a row of data
# conn.execute("INSERT INTO sqlmap_service VALUES ('activated')")
# conn.execute("INSERT INTO sqlmap_service VALUES ('deactivated')")

# Save (commit) the changes
# conn.commit()

# We can also close the connection if we are done with it.
# Just be sure any changes have been committed or they will be lost.
# conn.close()

# class Download(Resource):
#     def get(self):
#         return AutoIndex(app, browse_root=os.path.curdir)

@app.route('/report/<path:filename>')
def report(filename):
    root_dir = os.getcwd()
    return send_from_directory(os.path.join(root_dir, 'reports', ''), filename)

class PlatformType(Resource):
    def get(self):
        conn = db_connect.connect()
        query = conn.execute("SELECT * FROM platform")
        return {'platform': [data[0] for data in query.cursor.fetchall()]}

class Scan(Resource):
    def __init__(self):
        self.conn = db_connect.connect()

    def generate_id(self):
        # m = md5()
        # m.update(b"MANTRA")
        return "MANTRA_"+str(random.getrandbits(32))

    def post(self):
        id = self.generate_id()
        title = request.json['title']
        url = request.json['url']
        description = request.json['description']
        user_token = request.json['user_token']
        print(url)
        cmd = 'python mantra_scan.py --id=%s --url=%s' % (id, url)
        cmd = cmd.split()
        cmd.append('--title="%s"' % title)
        cmd.append('--description="%s"' % description)
        cmd.append('--user_token="%s"' % user_token)
        platform_id = Checker(url).platform_check()[0]
        platform_type = Checker(url).platform_check()[1]
        cmd.append('--platform_id=%s' % platform_id)
        cmd.append('--platform_type=%s' % platform_type)
        # try:
        #     result = check_output(cmd.split(), stderr=STDOUT, universal_newlines=True)
        # except Exception as e:

        #     result = e
        # print(dir(User))
        user_id = self.conn.execute("""
            SELECT id FROM user WHERE user_token = ?
        """, user_token).fetchone()[0]
        query = self.conn.execute("""
        INSERT INTO scanned_web (date_created, scan_id, title, url, description, status, raw_result, user_id, platform_id) VALUES (?,?,?,?,?,?,?,?,?);
        """, (datetime.datetime.now(), id, title, url, description, 0, '-', user_id, platform_id))
        result = check_output(cmd, stderr=STDOUT, universal_newlines=True)
        print(result)
        return {'data': result}

class SQLMapServiceStatus(Resource):
    def __init__(self):
        self.conn = db_connect.connect()
        self.query = self.conn.execute("SELECT status FROM sqlmap_service")
        self.result = {'status':self.query.fetchone()[0]}
        
    def post(self):
        print(request.json)
        status = request.json['status']
        if status == 'activated':
            # cmd = config['runner']['sqlmap'].split()
            # print(cmd)
            try:
                # Popen(cmd, stdout=PIPE, stderr=STDOUT)
                # self.result['status'] = 'on'
                if SQLMapService().service_checker() != 0:
                    print('Starting sqlmap api ...')
                    pool = ThreadPool(processes=1)
                    pool.apply_async(SQLMapService().start_sqlmap, ()).get()
                    self.query = self.conn.execute("UPDATE sqlmap_service SET status = 'activated' WHERE status = 'deactivated'")
                # print(self.result)
            except Exception as e:
                print("Error: %s" % (e))
        else:
            self.result['status'] = 'error'
        return self.result

    def get(self):
        if SQLMapService().service_checker() == 111:
            # self.result['status'] = 'off'
            self.query = self.conn.execute("UPDATE sqlmap_service SET status = 'deactivated' WHERE status = 'activated'")
            print(self.result)
        return jsonify(self.result)

class ScannedURL(Resource):
    def __init__(self):
        self.conn = db_connect.connect()

    def get(self):
        self.query = self.conn.execute("""
        SELECT scanned_web.title, scanned_web.url, scanned_web.description, scanned_web.status, scanned_web.scan_id,
        scanned_web.date_created, scanned_web.date_modified, user.username, platform.type FROM scanned_web 
        INNER JOIN user ON scanned_web.user_id = user.id
        INNER JOIN platform ON scanned_web.platform_id = platform.id ORDER BY scanned_web.id DESC
        """)
        # print(self.query.fetchall())
        result = {}
        columns = [d[0] for d in self.query.cursor.description]
        result['data'] = [dict(zip(columns, row)) for row in self.query.cursor.fetchall()]
        return jsonify(result)

class ScannedURLDetail(Resource):
    def __init__(self):
        self.conn = db_connect.connect()

    def get(self, scan_id):
        # self.query = self.conn.execute("SELECT * FROM scanned_web WHERE scan_id = ?", (scan_id))
        self.query = self.conn.execute("""
        SELECT scanned_web.title, scanned_web.url, scanned_web.description, scanned_web.raw_result, scanned_web.status, scanned_web.scan_id,
        scanned_web.date_created, scanned_web.date_modified, user.username, platform.type FROM scanned_web 
        INNER JOIN user ON scanned_web.user_id = user.id
        INNER JOIN platform ON scanned_web.platform_id = platform.id
        WHERE scan_id = ?;
        """, (scan_id))
        # print(self.query.fetchall())
        result = {}
        columns = [d[0] for d in self.query.cursor.description]
        result['data'] = [dict(zip(columns, row)) for row in self.query.cursor.fetchall()]
        final_result = {}
        final_result['description'] = result['data'][0]['description']
        final_result['status'] = result['data'][0]['status']
        final_result['raw_result'] = loads(result['data'][0]['raw_result'])
        final_result['scan_id'] = result['data'][0]['scan_id']
        final_result['title'] = result['data'][0]['title']
        final_result['type'] = result['data'][0]['type']
        final_result['url'] = result['data'][0]['url']
        final_result['username'] = result['data'][0]['username']
        # final_result.update(result['data'][0]['description'])
        return jsonify(final_result)

api.add_resource(PlatformType, '/platforms')
api.add_resource(Scan, '/scan')
api.add_resource(SQLMapServiceStatus, '/start_sqlmap')
api.add_resource(ScannedURL, '/scanned_url/all')
api.add_resource(ScannedURLDetail, '/scanned_url/detail/<scan_id>')

if __name__ == '__main__':
     app.run(port=int(config['service']['flask_port']))

# class Employees(Resource):
#     def get(self):
#         conn = db_connect.connect() # connect to database
#         query = conn.execute("select * from employees") # This line performs query and returns json result
#         return {'employees': [i[0] for i in query.cursor.fetchall()]} # Fetches

# class PlatformType(Resource):
#     def get(self, employee_id):
#         conn = db_connect.connect()
#         query = conn.execute("select * from employees where EmployeeId =%d "  %int(employee_id))
#         result = {'data': [dict(zip(tuple (query.keys()) ,i)) for i in query.cursor]}
#         return jsonify(result)