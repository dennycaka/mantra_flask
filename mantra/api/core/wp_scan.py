#!/usr/bin/python3

from subprocess import check_output, CalledProcessError, Popen, STDOUT, PIPE
import configparser
import os
from db.db import DBConnect
import base64
import json
from core.generate_pdf_reports import GeneratePDF
import random
import datetime

db_connect = DBConnect()

class WPScan:
    def __init__(self, url, scan_id):
        self.url = url
        self.scan_id = scan_id
        self.config = configparser.ConfigParser()
        self.full_path = os.path.dirname(os.path.abspath(__file__))
        self.root_path = os.path.join(self.full_path, '../')
        self.config.read(os.path.join(self.root_path, 'config.ini'))
        self.result = ""
        self.conn = db_connect.connect()
        self.wp_scan = self.wp_scan()
        self.updateScanRecord(self.wp_scan.strip())

    def run_scan(self, cmd):
        try:
            self.result = check_output(cmd, stderr=STDOUT, universal_newlines=True)
            return self.result
        except CalledProcessError as e:
            # print("[ERROR]", e.returncode, e.output)
            # if jsono:
            #     print("coeg")
            #     return json.dumps(json.loads(e.output.strip()))
            # loaded_json = json.loads(e.output.strip())
            # for x in loaded_json:
            #     print(x)
            # findings = loaded_json['interesting_findings']
            # for x in findings:
            #     data = [i for i in x]
            # print(loaded_json[x])
            # print("josss")
            # print(type(json.dumps(json.loads(e.output.strip()).keys(), sort_keys=True)))

            return e.output.strip()

    def updateScanRecord(self, result):
        query = """
        UPDATE scanned_web SET raw_result = ?, status = ?, date_modified = ? WHERE scan_id = ?
        """
        self.conn.execute(query, (result, 1, datetime.datetime.now(), self.scan_id))
        # print("%s %s %s %s" % (self.scan_id, self.scan_title, self.scan_url, self.scan_description))

    def wp_scan(self):
        scan = self.config['cms']['wordpress'] % (self.url, self.url)
        return self.run_scan(scan.split())