#!/usr/bin/python3

from multiprocessing.pool import ThreadPool
from subprocess import check_output, CalledProcessError, Popen, STDOUT, PIPE
import requests
import socket
import json
from core.wp_scan import WPScan
from core.joom_scan import JoomScan
from core.sqlmap_scan import SQLMapScan

class Scanner:
    def __init__(self, scan_id, scan_title, scan_url, scan_description, platform_type, scan_user_token, platform_id):
        self.scan_id = scan_id
        self.scan_title = scan_title
        self.scan_url = scan_url
        self.scan_description = scan_description
        self.platform_type = platform_type
        self.scan_user_token = scan_user_token
        self.platform_id = platform_id
        
    def scan(self):
        pool = ThreadPool(processes=1)
        if self.platform_type == 'wordpress':
            async_result = pool.apply_async(WPScan, (self.scan_url, self.scan_id,))
        elif self.platform_type == 'joomla':
            async_result = pool.apply_async(JoomScan, (self.scan_url, self.scan_id,))
        else:
            async_result = pool.apply_async(self.start_sqli_process, (self.scan_url,))
        result = async_result.get()
        print(result)
        return result