#!/usr/bin/python3

class GeneratePDF(object):
    def __init__(self, savedFileLocation, result):
        self.savedFileLocation = savedFileLocation
        self.result = result
    
    def generate_pdf(self):
        open(self.savedFileLocation, "w").write(self.result)