import sqlite3

conn = sqlite3.connect('example.db')
c = conn.cursor()
c.execute('''CREATE TABLE scanned_web 
    (scan_id text, scan_title text, scan_description text, scan_result text, scan_date text)''')

conn.commit()
conn.close()

# scan_id, scan_title, scan_description, result, date
